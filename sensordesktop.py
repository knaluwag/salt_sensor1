import threading
from sensor import app  # Import your Flask app

# The run_flask function doesn't need to change; it's only starting the Flask server.
def run_flask():
    app.run()

# Wrap the webview import and usage in a check for the 'CI' environment variable.
if __name__ == '__main__':
    t = threading.Thread(target=run_flask)
    t.daemon = True
    t.start()
    
    # Check if the CI environment variable is set
    if 'CI' not in os.environ:
        import webview
        webview.create_window('Sensor Data Dashboard', 'http://localhost:5000')
        webview.start()
    else:
        print("Running in CI environment, skipping webview window creation.")

import unittest
from sensor import app

class SensorTestCase(unittest.TestCase):

    def setUp(self):
        # Set up the Flask test client
        self.app = app.test_client()
        # Other setup can go here

    def tearDown(self):
        # Clean up code can go here
        pass

    def test_store_sensor_data(self):
        # Send a post request to the store-sensor-data endpoint
        response = self.app.post('/store-sensor-data', data='125')
        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)
        # Check that the response body contains the correct text.
        self.assertEqual(response.data.decode('utf-8'), "Data stored successfully")

# Add more tests for the other endpoints

if __name__ == '__main__':
    unittest.main()

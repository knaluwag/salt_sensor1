from sensordesktop import run_flask
import os

if __name__ == '__main__':
    run_flask()
    if 'CI' not in os.environ:
        import webview
        webview.create_window('Sensor Data Dashboard', 'http://localhost:5000')
        webview.start()